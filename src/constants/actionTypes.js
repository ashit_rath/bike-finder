const actionTypes = {};

[
  'GET_ALL_BIKES',
  'GET_LAT_LONG'
].forEach(action => actionTypes[action] = action)

export default Object.freeze(actionTypes)
