import React from 'react';
import { Route, IndexRoute } from 'react-router';

import App from 'components/app';
import Index from 'containers/index';

export default (
  <Route path="/" component={App}>
    <IndexRoute component={Index} />
  </Route>
);
