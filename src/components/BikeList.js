import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {GridList, GridTile} from 'material-ui/GridList';
import IconButton from 'material-ui/IconButton';
import LocationOn from 'material-ui/svg-icons/communication/location-on';

const styles = {
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
  },
  gridList: {
    width: 500,
    height: 550,
    overflowY: 'auto',
  },
};

class BikeList extends Component {
  constructor() {
    super()

    this.state = {}
  }

  onBikeLocationClick = (event) => {
    this.props.onLocationClick(event)
  }

  renderList = (bikes) => {
    return bikes.map(bike => {
      const { product_code, display_name, image: { vehicle_images }, display_price, current_location_name, current_city_name } = bike,
            { image, tag } = vehicle_images[0],
            address = `${current_location_name}, ${current_city_name}`;

      return (
        <GridTile
          key={product_code}
          title={display_name}
          subtitle={<span>Rs. <b>{display_price}</b></span>}
          actionIcon={<IconButton onClick={() => this.onBikeLocationClick(address)}><LocationOn color="white" /></IconButton>}
        >
          <img src={image} alt={tag} />
        </GridTile>
      );
    })
  }

  render() {

    if (!this.props.bikes.length)
      return (<div></div>);

    return (
      <GridList
        cellHeight={180}
        style={styles.gridList}
      >
       {this.renderList(this.props.bikes)}
      </GridList>
    );
  }
}

BikeList.propTypes = {
  bikes: PropTypes.array.isRequired,
  onLocationClick: PropTypes.func.isRequired
}

export default BikeList;
