import React from 'react';
import PropTypes from 'prop-types';
import GoogleMapReact from 'google-map-react'
import IconButton from 'material-ui/IconButton';
import LocationOn from 'material-ui/svg-icons/communication/location-on';

const AnyReactComponent = ({ text }) => <IconButton><LocationOn color="black" /></IconButton>;

const BikeLocation = (props) => {
  const { lat, long } = props

  return (
    <GoogleMapReact
      bootstrapURLKeys={{
        key: "AIzaSyA8u45sHDsAZMahkKQUHEI1mpgMaaAe7XU",
        language: 'en',
        region: 'in',
      }}
      defaultCenter={{ lat: 19.0760, lng: 72.8777 }}
      defaultZoom={13}
      center={[lat, long]}
    >
      <AnyReactComponent
        lat={lat}
        lng={long}
      />
    </GoogleMapReact>
  )
}

BikeLocation.propTypes = {
  lat: PropTypes.string.isRequired,
  long: PropTypes.string.isRequired
}

export default BikeLocation
