import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import AppBar from 'material-ui/AppBar';

import bikeActions from 'actions/bikeActions';
import BikeList from 'components/BikeList';
import BikeLocation from 'components/BikeLocation';

class Index extends Component {
  componentDidMount() {
    this.props.actions.getAllBikes()
  }

  onLocationClick = (address) => {
    this.props.actions.getLatLong(address)
  }

  render() {
    const { lat, long } = this.props.bike.currentLocation

    return (
      <div>
        <AppBar
          title="Bike Finder"
          iconClassNameRight="muidocs-icon-navigation-expand-more"
        />
        <div className="element-spacer-top-30"> </div>

        <div className="container">
          <div className="row">
            <div className="col-md-6">
              <BikeList bikes={this.props.bike.bikes} onLocationClick={this.onLocationClick}/>
            </div>
            <div className="col-md-6">
              <BikeLocation lat={lat} long={long} />
            </div>
          </div>
        </div>
      </div>
    )
  }
}

function mapStateToProps({ bike }) {
  return { bike };
}

function mapDispatchToProps(dispatch) {
  return { actions: bindActionCreators(Object.assign({}, bikeActions), dispatch) };
}

export default connect(mapStateToProps, mapDispatchToProps)(Index);
