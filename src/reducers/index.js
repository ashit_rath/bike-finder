import { combineReducers } from 'redux';

import bike from 'reducers/bike';

const rootReducer = combineReducers({
  bike
});

export default rootReducer;
