import { handleActions } from 'redux-actions'
import types from 'constants/actionTypes'

const reducer = {}
const initialState = {
  bikes: [],
  currentLocation: {
    lat: '19.0760',
    long: '72.8777'
  }
}

reducer[`${types.GET_ALL_BIKES}_FINISH`] = (state, action) => {
  return Object.assign({}, state, { bikes: action.data.payload.results});
}

reducer[`${types.GET_LAT_LONG}`] = (state, action) => {
  const { lat, lng } = action.data.results[0].geometry.location

  return Object.assign({}, state, { currentLocation: { lat: lat, long: lng }})
}

export default handleActions(reducer, initialState);
