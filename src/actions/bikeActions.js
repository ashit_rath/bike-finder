import axios from 'axios'
import google from '@google/maps'

import types from 'constants/actionTypes'

function getAllBikes() {
  return (dispatch) => {
    dispatch({ type: types.GET_ALL_BIKES })

    axios.get('https://api.credr.com/v1/product/search/?q=eyJwYWdlIjoxLCJjdXJyZW50X2NpdHlfaWQiOjJ9')
      .then(response => {
        dispatch({ type: `${types.GET_ALL_BIKES}_FINISH`, data: response.data })
      })
      .catch(error => {

      })
  }
}

function getLatLong(address) {
  const url = `https://maps.googleapis.com/maps/api/geocode/json?address=${address}`

  return (dispatch) => {
    axios.get(url)
      .then(response => {
        dispatch({ type: `GET_LAT_LONG`, data: response.data})
      })
  }
}

export default {
  getAllBikes,
  getLatLong
}
